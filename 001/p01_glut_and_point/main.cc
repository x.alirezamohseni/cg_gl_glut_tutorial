/**
   Author: AliReza Mohseni
   Email : x.alirezamohseni@gmail.com
   
   First glut/GL application example that shows a small point in red color at the
   center of the screen using `GL_POINTS` drawing mode.

   Build dependencies:
   	- Freeglut
	- OpenGL
	- glibc
	- stdc++
   
   For compile the code using meson use flowing command:
   	$ meson build
	$ cd build
	$ ninja
	$ ./main
   For compile using `g++` or `clang++` on unix like systems use flowing commands:
	for `g++(gcc)`:
   	$ g++ -lglut -lGL main.cc -o main
	$ ./main
	for `clang++(llvm)`:
	$ clang++ -lglut -lGL main.cc -o main
	$ ./main
   
 */
#include <GL/freeglut_std.h>
#include <GL/gl.h>
#include <GL/glut.h>

using namespace std;


void init(int *argc, char **argv);
void display(void);



int
main(int argc, char **argv)
{
  init(&argc, argv);
  glutDisplayFunc(display);

  glutMainLoop();
  return EXIT_SUCCESS;
}


void init(int *argc, char **argv)
{
  glutInit(argc, argv);
  glutCreateWindow("Tutorial 001");
  glClearColor(0.2,0.3,0.1,1.0);
}

void display()
{
  glClear(GL_COLOR_BUFFER_BIT);
  glColor3f(1.0,0.0,0.0);
  glPointSize(10);
  glBegin(GL_POINTS);
  glVertex2d(0,0);
  glEnd();
  glFlush();
}
